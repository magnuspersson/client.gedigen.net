/* tslint:disable */
/* eslint-disable */
/**
 * Porttelefon API
 * Release 2022-06-29-0840
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface VirtualNumber
 */
export interface VirtualNumber {
    /**
     * 
     * @type {string}
     * @memberof VirtualNumber
     */
    phone: string;
    /**
     * 
     * @type {number}
     * @memberof VirtualNumber
     */
    provider: number | null;
    /**
     * 
     * @type {number}
     * @memberof VirtualNumber
     */
    id: number;
    /**
     * 
     * @type {string}
     * @memberof VirtualNumber
     */
    createdAt: string;
    /**
     * 
     * @type {string}
     * @memberof VirtualNumber
     */
    deletedAt: string | null;
    /**
     * 
     * @type {string}
     * @memberof VirtualNumber
     */
    updatedAt: string | null;
}

export function VirtualNumberFromJSON(json: any): VirtualNumber {
    return VirtualNumberFromJSONTyped(json, false);
}

export function VirtualNumberFromJSONTyped(json: any, ignoreDiscriminator: boolean): VirtualNumber {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'phone': json['phone'],
        'provider': json['provider'],
        'id': json['id'],
        'createdAt': json['createdAt'],
        'deletedAt': json['deletedAt'],
        'updatedAt': json['updatedAt'],
    };
}

export function VirtualNumberToJSON(value?: VirtualNumber | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'phone': value.phone,
        'provider': value.provider,
        'id': value.id,
        'createdAt': value.createdAt,
        'deletedAt': value.deletedAt,
        'updatedAt': value.updatedAt,
    };
}

