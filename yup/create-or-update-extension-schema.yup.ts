import { __ } from 'index';
import { phone } from 'phone';
import * as yup from 'yup';

export const createOrUpdateExtensionSchema = yup.object({
  extension: yup.object({
    accountId: yup.number().defined(),
    locationId: yup.number().defined(),
    virtualNumberId: yup.number().defined().nullable(),
    name: yup
      .string()
      .defined(() => __('YUP_REQUIRED', { field: __('YUP_FIELD_NAME') }))
      .max(100, () =>
        __('YUP_MAX_LENGTH', { field: __('YUP_FIELD_NAME'), max: 100 }),
      ),
    position: yup
      .string()
      .max(20, () =>
        __('YUP_MAX_LENGTH', { field: __('YUP_FIELD_POSITION'), max: 20 }),
      ),
    phone: yup
      .string()
      .defined(() => __('YUP_REQUIRED', { field: __('YUP_FIELD_PHONE') }))
      .test(
        'Valid phone number',
        () => __('YUP_INVALID', { field: __('YUP_FIELD_PHONE') }),
        (value: any): boolean =>
          (
            phone(value || '', {
              country: 'SWE',
              validateMobilePrefix: false,
            }).phoneNumber || ''
          ).length > 0,
      ),
  }),
});
