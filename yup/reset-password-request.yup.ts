import { __ } from 'index';
import * as yup from 'yup';

export const resetPasswordSchema = yup.object({
  password: yup
    .string()
    .min(8, () =>
      __('YUP_MIN_LENGTH', { field: __('YUP_FIELD_PASSWORD'), min: 8 }),
    ),
});
