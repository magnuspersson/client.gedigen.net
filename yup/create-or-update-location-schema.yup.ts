import { __ } from 'index';
import { phone } from 'phone';
import * as yup from 'yup';

export const createOrUpdateLocationSchema = yup.object({
  accountId: yup.number().defined(),
  address: yup
    .string()
    .defined(() => __('YUP_REQUIRED', { field: __('YUP_FIELD_NAME') }))
    .max(45, () =>
      __('YUP_MAX_LENGTH', { field: __('YUP_FIELD_NAME'), max: 45 }),
    ),
  postcode: yup
    .string()
    .defined(() => __('YUP_REQUIRED', { field: __('YUP_FIELD_NAME') }))
    .max(45, () =>
      __('YUP_MAX_LENGTH', { field: __('YUP_FIELD_NAME'), max: 45 }),
    ),
  city: yup
    .string()
    .defined(() => __('YUP_REQUIRED', { field: __('YUP_FIELD_NAME') }))
    .max(45, () =>
      __('YUP_MAX_LENGTH', { field: __('YUP_FIELD_NAME'), max: 45 }),
    ),
  phone: yup
    .string()
    .defined(() => __('YUP_REQUIRED', { field: __('YUP_FIELD_PHONE') }))
    .test(
      'Valid phone number',
      () => __('YUP_INVALID', { field: __('YUP_FIELD_PHONE') }),
      (value: any): boolean =>
        (
          phone(value || '', {
            country: 'SWE',
            validateMobilePrefix: false,
          }).phoneNumber || ''
        ).length > 0,
    ),
});
