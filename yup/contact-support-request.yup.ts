import { __ } from 'index';
import * as yup from 'yup';

export const contactSupportSchema = yup.object({
  email: yup
    .string()
    .email()
    .required(() => __('YUP_REQUIRED', { field: __('YUP_FIELD_EMAIL') })),
  name: yup
    .string()
    .required()
    .min(2, () => __('YUP_REQUIRED', { field: __('YUP_FIELD_NAME') })),
  message: yup
    .string()
    .required()
    .min(2, () => __('YUP_REQUIRED', { field: __('YUP_FIELD_MESSAGE') })),
});
